//
//  TopStory.swift
//  NYC-TopStories
//
//  Created by Dimitris Papaioannou on 14/3/20.
//  Copyright © 2020 Dimitris Papaioannou. All rights reserved.
//

import Foundation
import ObjectMapper

class TopStory: Mappable {
    
    var num_results: Int?
    var results: [TopStoryResults]?

    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        num_results     <- map["num_results"]
        results         <- map["results"]
    }
    
}

