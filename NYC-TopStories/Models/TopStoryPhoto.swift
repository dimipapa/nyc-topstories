//
//  TopStoryPhoto.swift
//  NYC-TopStories
//
//  Created by Dimitris Papaioannou on 14/3/20.
//  Copyright © 2020 Dimitris Papaioannou. All rights reserved.
//

import Foundation
import ObjectMapper

class TopStoryPhoto: Mappable {
    
    var url: String?
    var format: String?
    var height: String?
    var width: String?
    var type: String?
    var subtype: String?
    var caption: String?
    var copyright: String?
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        url         <- map["url"]
        format      <- map["format"]
        height      <- map["height"]
        width       <- map["width"]
        type        <- map["type"]
        subtype     <- map["subtype"]
        caption     <- map["caption"]
        copyright   <- map["copyright"]
    }
    
}
