//
//  TopStoryResults.swift
//  NYC-TopStories
//
//  Created by Dimitris Papaioannou on 14/3/20.
//  Copyright © 2020 Dimitris Papaioannou. All rights reserved.
//

import Foundation
import ObjectMapper

class TopStoryResults: Mappable {
    
    var title: String?
    var abstract: String?
    var published_date: String?
    var uri: String?
    var article_url: String?
    var multimedia: [TopStoryPhoto]?

    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        title               <- map["title"]
        published_date      <- map["published_date"]
        uri                 <- map["uri"]
        abstract            <- map["abstract"]
        article_url         <- map["url"]
        multimedia          <- map["multimedia"]
    }
    
}
