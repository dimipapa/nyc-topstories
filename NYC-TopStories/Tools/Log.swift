//
//  Log.swift
//  NYC-TopStories
//
//  Created by Dimitris Papaioannou on 14/3/20.
//  Copyright © 2020 Dimitris Papaioannou. All rights reserved.
//

import Foundation
import os

private let subsystem = "com.dimitrispapa.nyctopstories"

struct Log {
    static let GetTopStories = OSLog(subsystem: subsystem, category: "GetTopStories")
    static let TopStories = OSLog(subsystem: subsystem, category: "TopStories")

}
