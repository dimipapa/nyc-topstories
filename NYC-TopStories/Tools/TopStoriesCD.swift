//
//  TopStoriesCD.swift
//  NYC-TopStories
//
//  Created by Dimitris Papaioannou on 15/3/20.
//  Copyright © 2020 Dimitris Papaioannou. All rights reserved.
//

import Foundation

struct TopStoriesCD {
    static let entity           = "TopStories"
    
    static let title            = "title"
    static let abstract         = "abstract"
    static let date             = "date"
    static let image            = "image"
    static let url              = "url"
    static let isBookmarked     = "isBookmarked"
}
