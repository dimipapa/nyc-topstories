//
//  DetailsViewController.swift
//  NYC-TopStories
//
//  Created by Dimitris Papaioannou on 14/3/20.
//  Copyright © 2020 Dimitris Papaioannou. All rights reserved.
//

import UIKit
import Kingfisher
import CoreData

enum Sender {
    case topStories
    case bookmarks
}

class DetailsViewController: UIViewController {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var storyImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var abstractLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var urlLabel: UILabel!
    @IBOutlet weak var bookmarkButton: UIButton!
    
    // MARK: - Properties
    
    var selectedIndexPath: Int?
    var topStories: [TopStories]?
    var bookmarks: [Bookmarks]?
    var url: URL?
    var cacheKey: String?
    var openedFrom: Sender?
    
    let viewModel = DetailsViewModel()
    
    
    // MARK: - Init
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupUI()
        setupImageView()
    }
    
    
    // MARK: - Methods
    
    func setupUI() {
        guard let index = selectedIndexPath else { return }
        
        if openedFrom == .topStories {
            titleLabel.text = topStories?[index].title
            abstractLabel.text = topStories?[index].abstract
            urlLabel.text = topStories?[index].article_url
            
            if let dateString = topStories?[index].date {
                dateLabel.text = viewModel.setupDate(dateToConvert: dateString)
            } else {
                dateLabel.text = "N/A"
            }
        } else {
            titleLabel.text = bookmarks?[index].title
            abstractLabel.text = bookmarks?[index].abstract
            urlLabel.text = bookmarks?[index].article_url
            dateLabel.text = bookmarks?[index].date
            bookmarkButton.isHidden = true
        }
    }
    
    func setupImageView() {
        guard let key = cacheKey else { return }

        // Check if image is cached
        let cache = ImageCache.default
        let cached = cache.isCached(forKey: key)
        
        // Check where the cached image is
        let cacheType = cache.imageCachedType(forKey: key)
        // `.memory`, `.disk` or `.none`.
        
        print(cache, cached, cacheType)
        
        cache.retrieveImage(forKey: key) { result in
            switch result {
            case .success(let value):
                print(value.cacheType)
                
                // If the `cacheType is `.none`, `image` will be `nil`.
                print(value.image as Any)
                
                if let validURL = self.url {
                    let resource = ImageResource(downloadURL: validURL, cacheKey: self.cacheKey)
                    self.storyImageView.kf.setImage(with: resource)
                }
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func saveStory() {
        print("Saving story")
        guard let managedContext = appDelegate?.persistentContainer.viewContext else { return }
        let story = Bookmarks(context: managedContext)

        story.title = titleLabel.text
        story.date = dateLabel.text
        story.abstract = abstractLabel.text
        story.article_url = urlLabel.text
        story.cache_key = cacheKey

        if let index = selectedIndexPath {
            story.url_main = topStories?[index].url_main
        }

        do {
            try managedContext.save()
        } catch {
            print("Could not save: \(error.localizedDescription)!")
        }
    }
    
    
    // MARK: - IBActions
    
    @IBAction func bookmarkButtonPressed(_ sender: Any) {
        saveStory()
    }
}
