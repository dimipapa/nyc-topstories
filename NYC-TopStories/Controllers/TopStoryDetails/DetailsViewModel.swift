//
//  DetailsViewModel.swift
//  NYC-TopStories
//
//  Created by Dimitris Papaioannou on 15/3/20.
//  Copyright © 2020 Dimitris Papaioannou. All rights reserved.
//

import Foundation
import os.log

class DetailsViewModel {

    // MARK: - Handlers
    
    func setupDate(dateToConvert: String) -> String {
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        let date = dateFormatter.date(from: dateToConvert)
        
        dateFormatter.dateFormat = "MM-dd-yyyy HH:mm"
        let showDate = dateFormatter.string(from: date!)
                
        return showDate
    }
    
}
