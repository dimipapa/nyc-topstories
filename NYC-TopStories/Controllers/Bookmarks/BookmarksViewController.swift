//
//  BookmarksViewController.swift
//  NYC-TopStories
//
//  Created by Dimitris Papaioannou on 14/3/20.
//  Copyright © 2020 Dimitris Papaioannou. All rights reserved.
//

import UIKit
import CoreData
import Kingfisher

class BookmarksViewController: UIViewController {

    // MARK: - IBOutlets

    @IBOutlet weak var collectionView: UICollectionView!
    

    // MARK: - Properties
    
    var stories: [Bookmarks] = []
    let reuseIdentifier = "cell"
    
    private let itemsPerRow: CGFloat = 2

    private let sectionInsets = UIEdgeInsets(top: 50.0,
                                             left: 10.0,
                                             bottom: 50.0,
                                             right: 10.0)


    // MARK: - Init

    override func viewDidLoad() {
        super.viewDidLoad()
        
//        removeBookmarks()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        fetchBookmarks()
        collectionView.reloadData()
    }
    
}


extension BookmarksViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return stories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        return CGSize(width: widthPerItem, height: widthPerItem)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! TopStoryCollectionViewCell
        
        if let key = stories[indexPath.item].cache_key {
            let cache = ImageCache.default
            
            cache.retrieveImage(forKey: key) { result in
                switch result {
                case .success(let value):
                    print(value.cacheType)
                    
                    // If the `cacheType is `.none`, `image` will be `nil`.
                    print(value.image as Any)
                    
                    let urlString = self.stories[indexPath.item].url_main
                    let url = URL(string: urlString ?? "")
                    
                    if let validUrl = url {
                        let resource = ImageResource(downloadURL: validUrl, cacheKey: self.stories[indexPath.item].cache_key)
                        cell.storyImageView.kf.setImage(with: resource)
                    }
                    
                case .failure(let error):
                    print(error)
                }
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Selected cell: \(indexPath.item)")
        
        let viewController = UIStoryboard.init(name: "Details", bundle: Bundle.main).instantiateViewController(withIdentifier: "DetailsViewController") as? DetailsViewController
        viewController?.selectedIndexPath = 0
        viewController?.openedFrom = .bookmarks
        viewController?.bookmarks = stories
        viewController?.url = URL(string: stories[indexPath.item].url_main ?? "")
        viewController?.cacheKey = stories[indexPath.item].cache_key
        self.navigationController?.pushViewController(viewController!, animated: true)
    }
    
}


extension BookmarksViewController {
    
    // MARK: - CoreData
    
    func fetchBookmarks() {
        guard let managedContext = appDelegate?.persistentContainer.viewContext else { return }
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Bookmarks")
        
        do {
            stories = try managedContext.fetch(fetchRequest) as! [Bookmarks]
            
            for storie in stories {
                print(storie.title as Any)
            }
        } catch {
            print("Could not fetch: \(error.localizedDescription)!")
        }
    }
    
    func removeBookmarks() {
        guard let managedContext = appDelegate?.persistentContainer.viewContext else { return }
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Bookmarks")
        
        // Create  Delete Request
        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        
        do {
            try managedContext.execute(batchDeleteRequest)
        } catch {
            print("Could not delete: \(error.localizedDescription)!")
        }
    }
    
}
