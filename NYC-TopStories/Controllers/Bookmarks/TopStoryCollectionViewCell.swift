//
//  TopStoryCollectionViewCell.swift
//  NYC-TopStories
//
//  Created by Dimitris Papaioannou on 15/3/20.
//  Copyright © 2020 Dimitris Papaioannou. All rights reserved.
//

import UIKit

class TopStoryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var storyImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }

}
