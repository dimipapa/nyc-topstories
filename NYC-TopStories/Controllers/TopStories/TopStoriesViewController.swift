//
//  TopStoriesViewController.swift
//  NYC-TopStories
//
//  Created by Dimitris Papaioannou on 14/3/20.
//  Copyright © 2020 Dimitris Papaioannou. All rights reserved.
//

import UIKit
import Kingfisher
import CoreData
import os.log

let appDelegate = UIApplication.shared.delegate as? AppDelegate

class TopStoriesViewController: UIViewController, UITableViewDelegate {
    
    // MARK: - IBOutlets

    @IBOutlet weak var tableView: UITableView!
    
    
    // MARK: - Properties
    
    let remoteRepository = RemoteRepository()
    let topStoryCellIdentifier = "TopStoryTableViewCell"
    var topStories: TopStory?
    var stories: [TopStories] = []
    
    lazy var viewModel: TopStoriesViewModel = {
        return TopStoriesViewModel()
    }()
    
    
    // MARK: - Init

    override func viewDidLoad() {
        super.viewDidLoad()
                
        fetchTopStories()

        initViewModel()
        setupTableView()
        viewModel.getTopStories()
    }
    
    
    // MARK: - Handlers
    
    func initViewModel() {
        viewModel.getTopStoriesSuccessClosure = { [weak self] stories in
            DispatchQueue.main.async {
                self?.removeTopStories()
                self?.saveTopStories(topStories: stories, completion: { saveCompleted in
                    if saveCompleted {
                        os_log("Save completed", log: Log.TopStories, type: .info)
                    }
                })
                self?.fetchTopStories()
                self?.tableView.reloadData()
            }
        }
        
        viewModel.getTopStoriesFailureClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.fetchTopStories()
            }
        }
    }

    func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: topStoryCellIdentifier, bundle: nil), forCellReuseIdentifier: topStoryCellIdentifier)
    }
    
}

extension TopStoriesViewController: UITableViewDataSource {
    
    // MARK: - TableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stories.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: TopStoryTableViewCell = tableView.dequeueReusableCell(withIdentifier: topStoryCellIdentifier, for: indexPath) as! TopStoryTableViewCell
        
        let url = URL(string: stories[indexPath.row].url_main ?? stories[indexPath.row].url_backup ?? "")
        // At indexPath.row[2] is the desired image size(thumbLarge), if this comes nil get standard thumbnail as an alternative, otherwise empty url string)
        
        let cacheKey = "image_cache_key" + String(indexPath.row)
        
        if let validURL = url {
            let resource = ImageResource(downloadURL: validURL, cacheKey: cacheKey)
            cell.storyImageView.kf.setImage(with: resource)
        }
    
        cell.titleLabel.text = stories[indexPath.row].title
        
        if let dateString = stories[indexPath.row].date {
            cell.dateLabel.text = viewModel.setupDate(dateToConvert: dateString)
        } else {
            cell.dateLabel.text = "N/A"
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let viewController = UIStoryboard.init(name: "Details", bundle: Bundle.main).instantiateViewController(withIdentifier: "DetailsViewController") as? DetailsViewController
        viewController?.selectedIndexPath = indexPath.row
        viewController?.openedFrom = .topStories
        viewController?.topStories = stories
        viewController?.url = URL(string: stories[indexPath.row].url_main ?? stories[indexPath.row].url_backup ?? "")
        viewController?.cacheKey = "image_cache_key" + String(indexPath.row)
        self.navigationController?.pushViewController(viewController!, animated: true)
    }
    
}

extension TopStoriesViewController {
    
    // MARK: - CoreData
    
    func saveTopStories(topStories: TopStory, completion: (_ finished: Bool) -> ()) {
        guard let count = topStories.results?.count else { return }
        var savedDataCount = 0

        for index in 0 ..< count {
            guard let managedContext = appDelegate?.persistentContainer.viewContext else { return }
            let stories = TopStories(context: managedContext)
            
            stories.title = topStories.results?[index].title
            stories.date = topStories.results?[index].published_date
            stories.abstract = topStories.results?[index].abstract
            stories.uri = topStories.results?[index].uri
            stories.article_url = topStories.results?[index].article_url
            stories.url_main = topStories.results?[index].multimedia?[2].url
            stories.url_backup = topStories.results?[index].multimedia?[1].url
            
            do {
                try managedContext.save()
                savedDataCount += 1
            } catch {
                print("Could not save: \(error.localizedDescription)!")
            }
        }
        
        if savedDataCount == count {
            completion(true)
        } else {
            completion(false)
        }
    }
    
    func fetchTopStories() {
        guard let managedContext = appDelegate?.persistentContainer.viewContext else { return }
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "TopStories")
        
        do {
            stories = try managedContext.fetch(fetchRequest) as! [TopStories]
            
            for story in stories {
                print(story.title as Any)
            }
        } catch {
            print("Could not fetch: \(error.localizedDescription)!")
        }
    }
    
    func removeTopStories() {
        guard let managedContext = appDelegate?.persistentContainer.viewContext else { return }
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "TopStories")
        
        // Create  Delete Request
        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        
        do {
            try managedContext.execute(batchDeleteRequest)
        } catch {
            print("Could not delete: \(error.localizedDescription)!")
        }
    }
    
}
