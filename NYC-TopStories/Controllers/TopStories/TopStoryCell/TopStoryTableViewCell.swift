//
//  TopStoryTableViewCell.swift
//  NYC-TopStories
//
//  Created by Dimitris Papaioannou on 14/3/20.
//  Copyright © 2020 Dimitris Papaioannou. All rights reserved.
//

import UIKit

class TopStoryTableViewCell: UITableViewCell {
    
    // MARK: - IBOutlets

    @IBOutlet weak var storyImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!

    
    // MARK: - Init

    override func awakeFromNib() {
        super.awakeFromNib()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }    
    
}
