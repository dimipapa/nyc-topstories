//
//  TopStoriesViewModel.swift
//  NYC-TopStories
//
//  Created by Dimitris Papaioannou on 14/3/20.
//  Copyright © 2020 Dimitris Papaioannou. All rights reserved.
//

import Foundation
import os.log

class TopStoriesViewModel {
    
    // MARK: - Properties

    private let remoteRepository = RemoteRepository()
    var topStories: TopStory?

    var getTopStoriesSuccessClosure: ((TopStory)->())?
    var getTopStoriesFailureClosure: (()->())?
    
    
    // MARK: - Init
    
    var getTopStoriesSuccessful: Bool = false {
        didSet {
            if let stories = topStories {
                self.getTopStoriesSuccessClosure?(stories)
            }
        }
    }
    
    var getTopStoriesFailure: Bool = false {
        didSet {
            self.getTopStoriesFailureClosure?()
        }
    }

    
    // MARK: - Handlers
    
    func getTopStories() {
        remoteRepository.getTopStoriesFromRemote(onSuccess: { [weak self] stories, code in
            self?.topStories = stories
            self?.getTopStoriesSuccessful = true
            
            if let sum = stories.num_results {
                os_log("Results sum: %d", log: Log.GetTopStories, type: .info, sum)
            }
        }, onFailure: { error in
            self.getTopStoriesFailure = true
            print(error)
        })
    }
    
    func setupDate(dateToConvert: String) -> String {
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        let date = dateFormatter.date(from: dateToConvert)
        
        dateFormatter.dateFormat = "MM-dd-yyyy HH:mm"
        let showDate = dateFormatter.string(from: date!)
                
        return showDate
    }
    
}
