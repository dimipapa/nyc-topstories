//
//  GetTopStoriesRepo.swift
//  NYC-TopStories
//
//  Created by Dimitris Papaioannou on 14/3/20.
//  Copyright © 2020 Dimitris Papaioannou. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
import os.log

class RemoteRepository {
    
    let getTopStoriesURL = "https://api.nytimes.com/svc/topstories/v2/home.json?api-key=vGmouEwo6Ly5NubPGx3yKRONb01GkEKa"
    
    // MARK: - Get current state
    
    typealias GetTopStoriesSuccess = (TopStory, _ statusCode: Int) -> ()
    typealias GetTopStoriesFailure = (Int) -> ()
    
    func getTopStoriesFromRemote(onSuccess: @escaping GetTopStoriesSuccess,
                                 onFailure: @escaping GetTopStoriesFailure) {
        
        AF.request(getTopStoriesURL,
                   method: .get,
                   encoding: JSONEncoding.default).responseJSON { response in
                    
                    os_log("GetTopStories Request: %s",    log: Log.GetTopStories, type: .info, String(describing: response.request))
                    os_log("GetTopStories Response: %s",   log: Log.GetTopStories, type: .info, String(describing: response.response))
                    os_log("GetTopStories Error: %@",      log: Log.GetTopStories, type: .info, String(describing: response.error))
                    os_log("GetTopStories Value: %@",      log: Log.GetTopStories, type: .info, String(describing: response.value))
                    
                    let code = response.response?.statusCode ?? 0
                    os_log("GetTopStories HTTP response code: %d", log: Log.GetTopStories, type: .info, code)
                    
                    switch response.result {
                    case .success:
                        if let JSON = Mapper<TopStory>().map(JSONObject: response.value) {
                            os_log("GetTopStories request successful, with code: %d", log: Log.GetTopStories, type: .info, code)
                            onSuccess(JSON, code)
                        }
                    case .failure(let error):
                        let errorCode = error._code
                        onFailure(errorCode)
                        os_log("GetTopStories eequest failed, with code %d and error: %s", log: Log.GetTopStories, type: .error, errorCode, String(describing: error.errorDescription))
                    }
        }
    }
    
}
